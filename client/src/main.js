import Vue from 'vue'
import App from './App'
import router from './router'

/**
 * Convert the date from date string.
 */
function fromDateString(dateString)
{
  let year = parseInt("20" + dateString.slice(0, 2));
  let month = parseInt(dateString.slice(2, 4));
  let day = parseInt(dateString.slice(4, 6));
  let date = new Date(year, month - 1, day);
  return date;
}

/**
 * Convert the date to date string.
 */
function toDateString(date)
{
  let yy = String(date.getFullYear()).slice(-2);
  let mm = ('00' + (date.getMonth() + 1)).slice(-2);
  let dd = ('00' + date.getDate()).slice(-2);
  let dateString = yy + mm + dd;
  return dateString;
}

/**
 * ex.: "123" -> "01:23", "0" -> "00:00", ...
 */
function decodeAbbreviatedTime(time)
{
  let hour = null;
  let minute = null;
  switch (time.length) {
  case 1:
    hour = "0" + time;
    minute = "00";
    break;
  case 2:
    hour = time;
    minute = "00";
    break;
  case 3:
    hour = "0" + time[0];
    minute = time.slice(1, 3);
    break;
  case 4:
    hour = time.slice(0, 2);
    minute = time.slice(2, 4);
    break;
  }
  return hour + ":" + minute + ":00";
}

Object.defineProperty(Vue.prototype, '$fromDateString', {'value': fromDateString});
Object.defineProperty(Vue.prototype, '$toDateString', {'value': toDateString});
Object.defineProperty(Vue.prototype, '$decodeAbbreviatedTime', {'value': decodeAbbreviatedTime});

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})


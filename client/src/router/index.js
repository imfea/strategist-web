import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/Login'
import NewTask from '@/components/NewTask'
import EditTask from '@/components/EditTask'
import Day from '@/components/Day'
import Today from '@/components/Today'
import NewQuote from '@/components/NewQuote'
import Graph from '@/components/Graph'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Login
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/today',
      name: 'Today',
      component: Today
    },
    {
      path: '/new-task/:day',
      name: 'NewTask',
      component: NewTask
    },
    {
      path: '/edit-task/:taskId',
      name: 'EditTask',
      component: EditTask
    },
    {
      path: '/day/:day',
      name: 'Day',
      component: Day
    },
    {
      path: '/new-quote',
      name: 'NewQuote',
      component: NewQuote
    },
    {
      path: '/graph',
      name: 'Graph',
      component: Graph
    }
  ]
})


class DependencyGraph
{
    constructor()
    {
        this._taskNodes = {};
        this._rootTaskId = null;
        this._showClosedTasks = true;
    }

    get rootTaskId()
    {
        return this._rootTaskId;
    }

    setGraph(graph, rootTaskId, showClosedTasks)
    {
        this._showClosedTasks = showClosedTasks;
        this._rootTaskId = rootTaskId;
        let levels = this.calcLevels(graph, rootTaskId);
        // TODO: Remove duplicated task ids!
        this._taskNodes = this.createTaskNodes(graph, levels);
    }

    calcLevels(graph, rootTaskId)
    {
        let levels = [];
        let level = [parseInt(rootTaskId)];
        while (level.length > 0) {
            levels.push(level);
            level = this.calcNextLevel(graph, level);
        }
        return levels;
    }

    calcNextLevel(graph, level)
    {
        let nextLevel = [];
        for (let taskId of level) {
            for (let predecessorId of graph["" + taskId]["predecessors"]) {
                if (nextLevel.includes(predecessorId) == false) {
                    let task = graph["" + predecessorId];
                    if (this.isClosedTask(task) == false || this._showClosedTasks == true) {
                        nextLevel.push(predecessorId);
                    }
                }
            }
        }
        return nextLevel;
    }

    isClosedTask(task)
    {
        if (task["finish"] == "None") {
            return false;
        }
        return true;
    }

    createTaskNodes(graph, levels)
    {
        const STEP_SIZE_X = 240;
        const STEP_SIZE_Y = 54;
        let taskNodes = {};
        let x = 0;
        for (let level of levels) {
            let y = -(level.length * STEP_SIZE_Y / 2);
            for (let taskId of level) {
                let task = graph[taskId];
                let position = new Point(x, y);
                taskNodes[taskId] = new TaskNode(task, position);
                y += STEP_SIZE_Y;
            }
            x -= STEP_SIZE_X;
        }
        // Successor tasks
        x = STEP_SIZE_X;
        let y = -STEP_SIZE_Y;
        for (let taskId of graph[this._rootTaskId]["successors"]) {
            let task = graph[taskId];
            let position = new Point(x, y);
            taskNodes[taskId] = new TaskNode(task, position);
            y += STEP_SIZE_Y;
        }
        return taskNodes;
    }

    findTaskAt(x, y)
    {
        for (const [taskId, taskNode] of Object.entries(this._taskNodes)) {
            if (taskNode.isUnderPosition(x, y)) {
                return taskId;
            }
        }
        return null;
    }

    draw(context)
    {
        this.drawEdges(context);
        this.drawNodes(context);
    }

    drawEdges(context)
    {
        for (const [taskId, taskNode] of Object.entries(this._taskNodes)) {
            for (let predId of taskNode.task["predecessors"]) {
                let targetNode = taskNode;
                let sourceNode = this._taskNodes[predId];
                this.drawEdge(context, sourceNode, targetNode);
            }
        }
    }

    drawEdge(context, sourceNode, targetNode)
    {
        if (sourceNode === undefined || targetNode === undefined) {
            return;
        }
        context.strokeStyle = "#222";
        context.lineWidth = 2;
        context.beginPath();
        context.moveTo(sourceNode.position.x + sourceNode.width, sourceNode.position.y + sourceNode.height / 2);
        context.lineTo(targetNode.position.x, targetNode.position.y + targetNode.height / 2);
        context.stroke()
    }

    drawNodes(context)
    {
        for (const [taskId, taskNode] of Object.entries(this._taskNodes)) {
            taskNode.draw(context);
        }
    }
}


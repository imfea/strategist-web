VUE_APP_BASE_URL = "/strategist"

/**
 * Class for managing dependency graph user interface
 */
class GraphView
{
    constructor(canvas, axios)
    {
        this._canvas = canvas;
        this._axios = axios;
        this._offset = new Point(0, 0);
        this._dragPosition = null;

        this._tasks = [];
        this._taskIndexOffset = 0;

        this._selectedTaskId = null;
        this._actuals = null;

        this._dependencyGraph = new DependencyGraph();

        this._operation = null;

        this._showClosedTasks = true;

        this._currentPage = "projects";
        this.loadProjects();
    }

    get selectedTaskId()
    {
        return this._selectedTaskId;
    }

    /**
     * Load the dependency graph from the server.
     */
    loadGraph(rootTaskId, needPositionReset)
    {
        console.log("Load graph .. " + rootTaskId);
        let request = {
          "method": "GET",
          "url": VUE_APP_BASE_URL + "/api/graph/" + rootTaskId,
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            let graph = result.data;
            this._dependencyGraph.setGraph(graph, rootTaskId, this._showClosedTasks);
            if (needPositionReset) {
                this._offset = new Point(300, 200);
            }
            this.draw();
          },
          error => {
            console.error(error);
          }
        );
    }

    /**
     * Load the active projects from the server.
     */
    loadProjects()
    {
        let request = {
          "method": "GET",
          "url": VUE_APP_BASE_URL + "/api/projects",
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            this._tasks = result.data;
            // this._dependencyGraph.setGraph(graph, rootTaskId);
            this._taskIndexOffset = 0;
            this.draw();
          },
          error => {
            console.error(error);
          }
        );
    }

    /**
     * Load the selected tasks from the server.
     */
    loadSelections()
    {
        let request = {
          "method": "GET",
          "url": VUE_APP_BASE_URL + "/api/selections",
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            this._tasks = result.data;
            this._taskIndexOffset = 0;
            this.draw();
          },
          error => {
            console.error(error);
          }
        );
    }

    /**
     * Load the latest tasks from the server.
     */
    loadLatests()
    {
        let request = {
          "method": "GET",
          "url": VUE_APP_BASE_URL + "/api/latests",
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            this._tasks = result.data;
            this._taskIndexOffset = 0;
            this.draw();
          },
          error => {
            console.error(error);
          }
        );
    }

    /**
     * Load the actual tasks from the server.
     */
    loadActuals()
    {
        let request = {
          "method": "GET",
          "url": VUE_APP_BASE_URL + "/api/actuals",
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            this._tasks = result.data;
            let actuals = [];
            for (let task of this._tasks) {
                actuals.push(task["id"]);
            }
            this._actuals = actuals;
            this._taskIndexOffset = 0;
            this.draw();
          },
          error => {
            console.error(error);
          }
        );
    }

    /**
     * Add the given task to the selection.
     */
    addToSelection(taskId)
    {
        if (typeof taskId == "string") {
            taskId = parseInt(taskId);
        }
        this._selectedTaskId = taskId;
        let request = {
          "method": "POST",
          "url": VUE_APP_BASE_URL + "/api/selections",
          "data": {
            "task_id": taskId
          },
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            if (this._currentPage == "selections") {
                this.loadSelections();
            }
          },
          error => {
            console.error(error);
          }
        );
    }

    clearSelection()
    {
        let request = {
          "method": "DELETE",
          "url": VUE_APP_BASE_URL + "/api/selections",
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            if (this._currentPage == "selections") {
                this.loadSelections();
            }
          },
          error => {
            console.error(error);
          }
        );
    }

    modifyEdge(operation, edgeType)
    {
        let request = {
          "method": "GET",
          "url": VUE_APP_BASE_URL + "/api/selections",
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            let selected_tasks = result.data;
            if (selected_tasks.length >= 2) {
                let sourceId = selected_tasks[1]["id"];
                let targetId = selected_tasks[0]["id"];
                let command = {
                    "operation": operation,
                    "source_id": sourceId,
                    "target_id": targetId,
                    "type": edgeType,
                };
                this.sendEdgeOperation(command);
            }
          },
          error => {
            console.error(error);
          }
        );
    }

    sendEdgeOperation(command)
    {
        let request = {
          "method": "POST",
          "url": VUE_APP_BASE_URL + "/api/edges",
          "data": command,
          "headers": {
            "content-type": "application/json"
          }
        };
        this._axios(request).then(
          result => {
            this.updateGraph();
          },
          error => {
            console.error(error);
          }
        );
    }

    modifyActuals(direction)
    {
        if (this._selectedTaskId != null && this._actuals != null) {
            let index = this._actuals.indexOf(this._selectedTaskId);
            if (index < 0) {
                console.log("Nincs!");
                if (direction == "down") {
                    this._actuals.unshift(this._selectedTaskId);
                }
                else if (direction == "up") {
                    this._actuals.push(this._selectedTaskId);
                }
                else if (direction == "remove") {
                    // Nothing to do here!
                }
                else {
                    // Invalid direction!
                }
            }
            else {
                console.log("Helyben!");
                if (direction == "down") {
                    if (index < this._actuals.length - 1) {
                        let temp = this._actuals[index + 1];
                        this._actuals[index + 1] = this._actuals[index];
                        this._actuals[index] = temp;
                    }
                }
                else if (direction == "up") {
                    if (index > 0) {
                        let temp = this._actuals[index - 1];
                        this._actuals[index - 1] = this._actuals[index];
                        this._actuals[index] = temp;
                    }
                }
                else if (direction == "remove") {
                    this._actuals.splice(index, 1);
                }
                else {
                    // Invalid direction!
                }
            }
            this.saveActuals();
        }
    }

    saveActuals()
    {
        if (this._actuals != null) {
            let request = {
              "method": "POST",
              "url": VUE_APP_BASE_URL + "/api/actuals",
              "data": this._actuals,
              "headers": {
                "content-type": "application/json"
              }
            };
            this._axios(request).then(
              result => {
                this.loadActuals();
              },
              error => {
                console.error(error);
              }
            );
        }
    }

    updateGraph()
    {
        let rootTaskId = this._dependencyGraph.rootTaskId;
        this.loadGraph(rootTaskId, false);
    }

    /**
     * Handle mouse down event.
     */
    onMouseDown(mouseEvent)
    {
        if (mouseEvent.button == 0) {
            if (mouseEvent.x < 200) {
                if (mouseEvent.y < 30) {
                    let iconIndex = Math.floor(mouseEvent.x / 20);
                    switch (iconIndex) {
                    case 1:
                        this._currentPage = "projects";
                        this.loadProjects();
                        break;
                    case 2:
                        this._currentPage = "actuals";
                        this.loadActuals();
                        break;
                    case 3:
                        this._currentPage = "latests";
                        this.loadLatests();
                        break;
                    case 4:
                        this._currentPage = "selections";
                        this.loadSelections();
                        break;
                    }
                }
                else {
                    let i = Math.floor((mouseEvent.y - 30) / 32);
                    let index = this._taskIndexOffset + i;
                    if (i >= 0 && index >= 0 && index < this._tasks.length) {
                        let taskId = this._tasks[index]["id"];
                        this.addToSelection(taskId);
                        /*
                        this._operation = {
                            "sourcePoint": new Point(200, i * 32 + 30 + 12),
                            "targetPoint": new Point(mouseEvent.x, mouseEvent.y),
                            "sourceTaskId": taskId
                        };
                        */
                    }
                }
            }
            else {
                let x = mouseEvent.x - this._offset.x;
                let y = mouseEvent.y - this._offset.y;
                let taskId = this._dependencyGraph.findTaskAt(x, y);
                if (taskId != null) {
                    this.addToSelection(taskId);
                }
            }
        }
        if (mouseEvent.button == 1) {
            this._dragPosition = new Point(mouseEvent.x, mouseEvent.y);
        }
        if (mouseEvent.button == 2) {
            if (mouseEvent.x < 200) {
                let i = Math.floor((mouseEvent.y - 30) / 32);
                let index = this._taskIndexOffset + i;
                if (i >= 0 && index >= 0 && index < this._tasks.length) {
                    let taskId = this._tasks[index]["id"];
                    this.loadGraph(taskId, true);
                }
            }
            if (mouseEvent.x > 200) {
                let x = mouseEvent.x - this._offset.x;
                let y = mouseEvent.y - this._offset.y;
                let taskId = this._dependencyGraph.findTaskAt(x, y);
                if (taskId != null) {
                    this.loadGraph(taskId, true);
                }
            }
        }
    }

    /**
     * Handle mouse move event.
     */
    onMouseMove(mouseEvent)
    {
        if (this._dragPosition != null) {
            let dx = mouseEvent.x - this._dragPosition.x;
            let dy = mouseEvent.y - this._dragPosition.y;
            this._offset.translate(dx, dy);
            this._dragPosition = new Point(mouseEvent.x, mouseEvent.y);
            this.draw();
        }
        if (this._operation != null) {
            this._operation["targetPoint"] = new Point(mouseEvent.x, mouseEvent.y);
            this.draw();
        }
    }

    /**
     * Handle mouse up event.
     */
    onMouseUp(mouseEvent)
    {
        this._dragPosition = null;
        if (this._operation != null) {
            if (mouseEvent.x > 200) {
                let x = mouseEvent.x - this._offset.x;
                let y = mouseEvent.y - this._offset.y;
                let targetTaskId = this._dependencyGraph.findTaskAt(x, y);
                if (targetTaskId != null) {
                    let sourceTaskId = this._operation["sourceTaskId"];
                    console.log("[[ " + sourceTaskId + " -> " + targetTaskId + " ]]");
                }
            }
            this._operation = null;
            this.draw();
        }
    }

    /**
     * Handle mouse wheel event.
     */
    onMouseWheel(mouseEvent, delta)
    {
        if (mouseEvent.x < 200 && mouseEvent.y > 30) {
            if (delta > 0) {
                --this._taskIndexOffset;
            }
            else {
                ++this._taskIndexOffset;
                if (this._taskIndexOffset > this._tasks.length - 1) {
                    this._taskIndexOffset = this._tasks.length - 1;
                }
            }
            if (this._taskIndexOffset < 0) {
                this._taskIndexOffset = 0;
            }
            this.draw();
        }
        if (mouseEvent.x > 200) {
            if (delta > 0) {
                this._offset.translate(0, 48);
            }
            else {
                this._offset.translate(0, -48);
            }
            this.draw();
        }
    }

    /**
     * Handle key down event.
     */
    onKeyDown(event)
    {
        switch (event.key) {
        case "c":
            this.clearSelection();
            break;
        case "a":
            this.modifyEdge("add", "dependency");
            break;
        case "d":
            this.modifyEdge("remove", "dependency");
            break;
        /*
        case "q":
            this.modifyEdge("add", "priority");
            break;
        case "e":
            this.modifyEdge("remove", "priority");
            break;
        */
        case "k":
            this.modifyActuals("down");
            break;
        case "l":
            this.modifyActuals("up");
            break;
        case "i":
            this.modifyActuals("up");
            break;
        case "o":
            this.modifyActuals("remove");
            break;
        case "h":
            if (this._showClosedTasks) {
                this._showClosedTasks = false;
            }
            else {
                this._showClosedTasks = true;
            }
            let rootTaskId = this._dependencyGraph.rootTaskId;
            this.loadGraph(rootTaskId, false);
        }
    }
    
    /**
     * Draw the contents of the graph view.
     */
    draw()
    {
        let context = this._canvas.getContext("2d");

        context.resetTransform();
        context.clearRect(0, 0, this._canvas.width, this._canvas.height);

        context.save();
        context.translate(this._offset.x, this._offset.y);
        this._dependencyGraph.draw(context);
        context.restore();

        this.drawIconBar(context);
        this.drawTasks(context);

        if (this._operation != null) {
            context.strokeStyle = "#08F";
            context.lineWidth = 3;
            context.beginPath();
            context.moveTo(this._operation["sourcePoint"].x, this._operation["sourcePoint"].y);
            context.lineTo(this._operation["targetPoint"].x, this._operation["targetPoint"].y);
            context.stroke()
        }
    }

    /**
     * Draw the icon bar.
     */
    drawIconBar(context)
    {
        context.fillStyle = "#CCC";
        for (let j = 0; j < 9; ++j) {
            context.fillRect(j * 20, 0, 3, 16);
        }
        context.fillStyle = "#000";
        context.font = "16px Monospace";
        context.fillText("O P Q R S F E D", 10, 14);

        if (this._showClosedTasks == false) {
            context.fillStyle = "#080";
            context.fillText("H!", 180, 14);
        }
    }

    calcTaskColor(task)
    {
        let color = "";
        if (task["start"] == "None" || task["start"] == null) {
            if (task["finish"] == "None" || task["finish"] == null) {
                color = "#EEE";
            }
            else {
                color = "#FDD";
            }
        }
        else {
            if (task["finish"] == "None" || task["finish"] == null) {
                color = "#FFD";
            }
            else {
                color = "#DFD";
            }
        }
        return color;
    }

    /**
     * Draw the tasks on the side panel.
     */
    drawTasks(context)
    {
        let x = 10;
        let y = 30;
        let width = 200;
        let height = 24;
        let dy = 32;

        let i = this._taskIndexOffset;
        while (i < this._tasks.length && y < 400) {
            let task = this._tasks[i];

            context.fillStyle = this.calcTaskColor(task);
            context.fillRect(x, y, width, height);

            context.beginPath();
            context.lineWidth = 1;
            context.strokeStyle = "#888";
            context.rect(x, y, width, height);
            context.stroke();

            context.fillStyle = "#000";
            context.font = "16px Verdana";
            context.fillText(task["name"], x + 6, y + 17);

            ++i;
            y += dy;
        }
    }
}


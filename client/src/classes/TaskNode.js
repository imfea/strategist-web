class TaskNode
{
    constructor(task, position)
    {
        this._task = task;
        this._position = position;
        this._width = 200;
        this._height = 40;
    }

    get task()
    {
        return this._task;
    }

    get position()
    {
        return this._position;
    }

    get width()
    {
        return this._width;
    }

    get height()
    {
        return this._height;
    }

    calcTaskColor(task)
    {
        let color = "";
        if (task["start"] == "None") {
            if (task["finish"] == "None") {
                color = "#EEE";
            }
            else {
                color = "#FDD";
            }
        }
        else {
            if (task["finish"] == "None") {
                color = "#FFD";
            }
            else {
                color = "#DFD";
            }
        }
        return color;
    }

    draw(context)
    {
        context.fillStyle = this.calcTaskColor(this._task);
        context.fillRect(this._position.x, this._position.y, this._width, this._height);
        
        context.beginPath();
        context.lineWidth = 1;
        context.strokeStyle = "#888";
        context.rect(this._position.x, this._position.y, this._width, this._height);
        context.stroke();

        context.fillStyle = "#000";
        context.font = "16px Verdana";
        
        let lines = this.breakLines(context, this._task["name"], this._width - 24);
        context.fillText(lines[0], this._position.x + 6, this._position.y + 16);
        context.fillText(lines[1], this._position.x + 6, this._position.y + 32);
    }
    
    /**
     * Break the text into two lines according to the current font of the context
     * and the maximal width.
     */
    breakLines(context, text, maxWidth)
    {
        let i = 0;
        let firstLine = "";
        while (i < text.length && context.measureText(firstLine).width < maxWidth) {
            firstLine = firstLine + text[i];
            ++i;
        }
        let secondLine = "";
        while (i < text.length && context.measureText(secondLine).width < maxWidth) {
            secondLine = secondLine + text[i];
            ++i;
        }
        return [firstLine, secondLine];
    }

    isUnderPosition(x, y)
    {
        if (x >= this._position.x
            && y >= this._position.y
            && x < this._position.x + this._width
            && y < this._position.y + this._height)
        {
            return true;
        }
        return false;
    }
}


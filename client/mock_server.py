import json

import falcon
import waitress


class LoginResource:
    def on_post(self, req, resp):
        data = json.loads(req.stream.read().decode('utf-8'))
        print(data)
        message = {
          "token": "ok"
        }
        resp.body = json.dumps(message)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class TaskResource:

    def on_get(self, req, resp, task_id):
        print(f'task_id = {task_id}')
        task_data = {
          'id': task_id,
          'name': 'Trial',
          'description': f'... {task_id}',
          'type': 'quest',
          'start': '10',
          'finish': '12'
        }
        resp.body = json.dumps(task_data)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        data = json.loads(req.stream.read().decode('utf-8'))
        resp.body = "ok"
        resp.status = falcon.HTTP_200

    def on_put(self, req, resp):
        data = json.loads(req.stream.read().decode('utf-8'))
        resp.body = "ok"
        resp.status = falcon.HTTP_200


class DayResource:

    def on_get(self, req, resp, day):
        print(f'day = {day}')
        tasks = [
            {
                "id": 1001,
                "time": "10-11",
                "type": "study",
                "name": "Seems to be"
            },
            {
                "id": 1002,
                "time": "11-12",
                "type": "documentation",
                "name": "Ok!"
            }
        ]
        resp.body = json.dumps(tasks)
        resp.status = falcon.HTTP_200


class QuoteResource:

    def on_post(self, req, resp):
        data = json.loads(req.stream.read().decode('utf-8'))
        resp.body = "ok"
        resp.status = falcon.HTTP_200


login_resource = LoginResource()
task_resource = TaskResource()
quote_resource = QuoteResource()
day_resource = DayResource()

app = falcon.API()

app.add_route('/login', login_resource)
app.add_route('/tasks', task_resource)
app.add_route('/tasks/{task_id:int}', task_resource)
app.add_route('/day/{day}', day_resource)
app.add_route('/quotes', quote_resource)

waitress.serve(app, host='127.0.0.1', port=5000)


import datetime
import unittest

from strategist.strategist import Strategist


class QueryTest(unittest.TestCase):
    """
    Test cases for task queries
    """

    @staticmethod
    def create_strategist_with_sample_tasks():
        strategist = Strategist('sqlite:///:memory:')
        for task_id in range(1, 50):
            task_data = {
                'name': f'Sample {task_id}',
                'description': 'Selection testing',
                'type': 'development',
                'start': None,
                'finish': None,
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            _ = strategist.create_task(task_data)
        strategist.create_register(user_id=1)
        return strategist

    def test_collect_latest_tasks(self):
        strategist = Strategist('sqlite:///:memory:')
        for task_id in range(1, 20):
            task_data = {
                'name': f'Sample {task_id}',
                'description': 'Query testing',
                'type': 'development',
                'start': None,
                'finish': None,
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            _ = strategist.create_task(task_data)
            latest_tasks = strategist.collect_latest_tasks()
            self.assertEqual(len(latest_tasks), task_id)
            for i, task_data in enumerate(reversed(latest_tasks)):
                self.assertEqual(task_data['id'], i + 1)

    def test_collect_active_projects(self):
        strategist = Strategist('sqlite:///:memory:')
        expected_tasks = []
        for task_id in range(1, 100):
            task_type = 'project' if task_id % 7 == 0 else 'development'
            task_data = {
                'id': task_id,
                'name': f'Sample {task_id}',
                'description': 'Query testing',
                'type': task_type,
                'start': None,
                'finish': None,
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            if task_type == 'project':
                expected_tasks.append(task_data)
            _ = strategist.create_task(task_data)
        active_projects = strategist.collect_active_projects()
        self.assertEqual(expected_tasks, active_projects)

    def test_collect_active_projects_without_results(self):
        strategist = Strategist('sqlite:///:memory:')
        for task_id in range(1, 100):
            task_data = {
                'name': f'Sample {task_id}',
                'description': 'Query testing',
                'type': 'freetime',
                'start': None,
                'finish': None,
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            _ = strategist.create_task(task_data)
        active_projects = strategist.collect_active_projects()
        self.assertEqual(active_projects, [])

    def test_collect_active_projects_with_inactive_projects(self):
        strategist = Strategist('sqlite:///:memory:')
        for task_id in range(1, 20):
            start_time = datetime.datetime(2020, 10, 6 + task_id, 12, 0, 0)
            finish_time = datetime.datetime(2020, 10, 6 + task_id, 13, 0, 0)
            task_data = {
                'name': f'Sample {task_id}',
                'description': 'Query testing',
                'type': 'project',
                'start': start_time if task_id % 3 > 0 else None,
                'finish': finish_time,
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            _ = strategist.create_task(task_data)
        active_projects = strategist.collect_active_projects()
        self.assertEqual(active_projects, [])

    def test_collect_selected_tasks(self):
        strategist = self.create_strategist_with_sample_tasks()
        strategist.select_task(2)
        strategist.select_task(3)
        strategist.select_task(5)
        selected_tasks = strategist.collect_selected_tasks()
        self.assertEqual(len(selected_tasks), 3)
        for i, task_id in enumerate([5, 3, 2]):
            self.assertEqual(selected_tasks[i]['id'], task_id)

    def test_collect_selected_tasks_without_selection(self):
        strategist = self.create_strategist_with_sample_tasks()
        selected_tasks = strategist.collect_selected_tasks()
        self.assertEqual(len(selected_tasks), 0)

    def test_collect_selected_tasks_with_deselect(self):
        strategist = self.create_strategist_with_sample_tasks()
        for task_id in range(10, 31):
            strategist.select_task(task_id)
        for task_id in range(12, 29):
            strategist.deselect_task(task_id)
        selected_tasks = strategist.collect_selected_tasks()
        self.assertEqual(len(selected_tasks), 4)
        for i, task_id in enumerate([30, 29, 11, 10]):
            self.assertEqual(selected_tasks[i]['id'], task_id)

    def test_collect_selected_tasks_with_clear_selection(self):
        strategist = self.create_strategist_with_sample_tasks()
        strategist.clear_task_selection()
        selected_tasks = strategist.collect_selected_tasks()
        self.assertEqual(len(selected_tasks), 0)

    def test_select_task_invalid_task_id(self):
        strategist = self.create_strategist_with_sample_tasks()
        try:
            strategist.select_task(1234)
        except ValueError as error:
            self.assertEqual(str(error), 'The task id 1234 is invalid!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_deselect_task_invalid_task_id(self):
        strategist = self.create_strategist_with_sample_tasks()
        try:
            strategist.select_task(1234)
        except ValueError as error:
            self.assertEqual(str(error), 'The task id 1234 is invalid!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_deselect_task_invalid_task_id_not_selected(self):
        strategist = self.create_strategist_with_sample_tasks()
        try:
            strategist.select_task(5)
            strategist.deselect_task(6)
        except ValueError as error:
            self.assertEqual(str(error), 'The task with id 6 has not selected!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_actuals(self):
        strategist = self.create_strategist_with_sample_tasks()
        expected_actuals = [5, 6, 7, 8]
        strategist.set_actuals(expected_actuals)
        actuals = strategist.get_actuals()
        self.assertEqual(actuals, expected_actuals)

    def test_actuals_empty(self):
        strategist = self.create_strategist_with_sample_tasks()
        actuals = strategist.get_actuals()
        self.assertEqual(actuals, [])

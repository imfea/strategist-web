import unittest

from strategist.strategist import Strategist


class EdgeTest(unittest.TestCase):
    """
    Test cases for edge operations
    """

    @staticmethod
    def create_strategist():
        strategist = Strategist('sqlite:///:memory:')
        for task_id in range(1, 7):
            task_data = {
                'name': f'Sample {task_id}',
                'description': 'Dependency testing',
                'type': 'development',
                'start': None,
                'finish': None,
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            _ = strategist.create_task(task_data)
        return strategist

    @staticmethod
    def add_sample_edges(strategist):
        edges = [
            (5, 3), (6, 3), (2, 1), (3, 1), (4, 1)
        ]
        for source_id, target_id in edges:
            strategist.add_edge(source_id, target_id)
        return strategist

    def test_without_edges(self):
        strategist = self.create_strategist()
        for task_id in range(1, 7):
            predecessors = strategist.collect_predecessors(task_id)
            self.assertEqual(predecessors, [])
            successors = strategist.collect_successors(task_id)
            self.assertEqual(successors, [])

    def test_add_edge(self):
        strategist = self.create_strategist()
        strategist.add_edge(2, 1)
        predecessors = strategist.collect_predecessors(1)
        self.assertEqual(predecessors, [2])
        successors = strategist.collect_successors(2)
        self.assertEqual(successors, [1])

    def test_sample_edges(self):
        strategist = self.create_strategist()
        strategist = self.add_sample_edges(strategist)
        expected_predecessors = {
            1: [2, 3, 4],
            2: [],
            3: [5, 6],
            4: [],
            5: [],
            6: []
        }
        expected_successors = {
            1: [],
            2: [1],
            3: [1],
            4: [1],
            5: [3],
            6: [3]
        }
        for task_id in range(1, 7):
            predecessors = strategist.collect_predecessors(task_id)
            self.assertEqual(predecessors, expected_predecessors[task_id])
            successors = strategist.collect_successors(task_id)
            self.assertEqual(successors, expected_successors[task_id])

    def test_add_edge_with_invalid_source_id(self):
        strategist = self.create_strategist()
        try:
            strategist.add_edge(8, 1)
        except ValueError as error:
            self.assertEqual(str(error), 'The task id 8 is invalid for adding edge!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_add_edge_with_invalid_target_id(self):
        strategist = self.create_strategist()
        try:
            strategist.add_edge(1, 8)
        except ValueError as error:
            self.assertEqual(str(error), 'The task id 8 is invalid for adding edge!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_add_edge_multiple_times(self):
        strategist = self.create_strategist()
        for _ in range(10):
            strategist.add_edge(2, 1)
            predecessors = strategist.collect_predecessors(1)
            self.assertEqual(predecessors, [2])
            successors = strategist.collect_successors(2)
            self.assertEqual(successors, [1])

    def test_remove_edge(self):
        strategist = self.create_strategist()
        strategist.add_edge(2, 1)
        strategist.remove_edge(2, 1)
        predecessors = strategist.collect_predecessors(1)
        self.assertEqual(predecessors, [])
        successors = strategist.collect_successors(2)
        self.assertEqual(successors, [])

    def test_remove_edge_with_invalid_id(self):
        strategist = self.create_strategist()
        try:
            strategist.add_edge(1, 8)
        except ValueError as error:
            self.assertEqual(str(error), 'The task id 8 is invalid for adding edge!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_remove_edge_missing(self):
        strategist = self.create_strategist()
        strategist = self.add_sample_edges(strategist)
        missing_edges = [
            (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6),
            (2, 2), (2, 3), (2, 4), (2, 5), (2, 6),
            (3, 2), (3, 3), (3, 4), (3, 5), (3, 6),
            (4, 2), (4, 3), (4, 4), (4, 5), (4, 6),
            (5, 1), (5, 2), (5, 4), (5, 5), (5, 6),
            (6, 1), (6, 2), (6, 4), (6, 5), (6, 6)
        ]
        for source_id, target_id in missing_edges:
            strategist.remove_edge(source_id, target_id)

    def test_collect_predecessors_invalid_id(self):
        strategist = self.create_strategist()
        try:
            _ = strategist.collect_predecessors(8)
        except ValueError as error:
            self.assertEqual(str(error), 'The task id 8 is invalid for collecting predecessors!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_collect_successors_invalid_id(self):
        strategist = self.create_strategist()
        try:
            _ = strategist.collect_successors(8)
        except ValueError as error:
            self.assertEqual(str(error), 'The task id 8 is invalid for collecting successors!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_self_reference_checking(self):
        strategist = self.create_strategist()
        strategist = self.add_sample_edges(strategist)
        for task_id in range(1, 7):
            try:
                strategist.add_edge(task_id, task_id)
            except ValueError as error:
                self.assertEqual(str(error), f'Unable to add edge {task_id} -> {task_id}!')
            else:
                self.fail('The expected ValueError has not raised!')

    def test_circular_dependency_checking(self):
        strategist = self.create_strategist()
        strategist = self.add_sample_edges(strategist)
        invalid_edges = [
            (1, 2), (1, 3), (1, 4), (1, 5), (1, 6),
            (3, 5), (3, 6)
        ]
        for source_id, target_id in invalid_edges:
            try:
                strategist.add_edge(source_id, target_id)
            except ValueError as error:
                self.assertEqual(str(error), f'Unable to add edge {source_id} -> {target_id}!')
            else:
                self.fail('The expected ValueError has not raised!')

    def test_calc_dependency_graph(self):
        strategist = self.create_strategist()
        strategist = self.add_sample_edges(strategist)
        expected_dependency_graph = {
            1: {
                'predecessors': [2, 3, 4],
                'successors': []
            },
            2: {
                'predecessors': [],
                'successors': [1]
            },
            3: {
                'predecessors': [5, 6],
                'successors': [1]
            },
            4: {
                'predecessors': [],
                'successors': [1]
            },
            5: {
                'predecessors': [],
                'successors': [3]
            },
            6: {
                'predecessors': [],
                'successors': [3]
            }
        }
        dependency_graph = strategist.calc_dependency_graph(1)
        self.assertEqual(dependency_graph, expected_dependency_graph)

    def test_calc_dependency_graph_smaller(self):
        strategist = self.create_strategist()
        strategist = self.add_sample_edges(strategist)
        expected_dependency_graph = {
            3: {
                'predecessors': [5, 6],
                'successors': [1]
            },
            5: {
                'predecessors': [],
                'successors': [3]
            },
            6: {
                'predecessors': [],
                'successors': [3]
            }
        }
        dependency_graph = strategist.calc_dependency_graph(3)
        self.assertEqual(expected_dependency_graph, dependency_graph)

    def test_calc_dependency_graph_invalid_id(self):
        strategist = self.create_strategist()
        try:
            _ = strategist.calc_dependency_graph(8)
        except ValueError as error:
            self.assertEqual(str(error), 'The task id 8 is invalid for calculating dependency graph!')
        else:
            self.fail('The expected ValueError has not raised!')

    # TODO: Check the task states before adding an edge!

    def test_collect_queue_targets(self):
        strategist = self.create_strategist()
        for source_id, target_id in [(5, 2), (6, 4)]:
            strategist.add_edge(source_id, target_id, type=model.PRIORITY)
        queue_targets = strategist.collect_queue_targets()
        self.assertEqual(len(queue_targets), 2)
        self.assertEqual(queue_targets[0].id, 2)
        self.assertEqual(queue_targets[1].id, 4)

    def test_collect_queue_targets_empty(self):
        strategist = self.create_strategist()
        queue_targets = strategist.collect_queue_targets()
        self.assertEqual(len(queue_targets), 0)

    def test_collect_queue_targets_duplicates(self):
        strategist = self.create_strategist()
        for source_id, target_id in [(5, 2), (6, 2)]:
            strategist.add_edge(source_id, target_id, type=model.PRIORITY)
        queue_targets = strategist.collect_queue_targets()
        self.assertEqual(len(queue_targets), 1)
        self.assertEqual(queue_targets[0].id, 2)

    # TODO: Check dependency and priority overrides!

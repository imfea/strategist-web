import datetime
import unittest

from strategist.strategist import Strategist


class TaskTest(unittest.TestCase):
    """
    Test cases for task operations
    """

    def test_create_task(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        task_id = strategist.create_task(task_data)
        self.assertEqual(task_id, 1)

    def test_get_task(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        task_id = strategist.create_task(task_data)
        retrieved_data = strategist.get_task(task_id)
        task_data['id'] = task_id
        self.assertEqual(retrieved_data, task_data)

    def test_create_task_invalid_type(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'invalid',
            'start': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        try:
            _ = strategist.create_task(task_data)
        except ValueError as error:
            self.assertEqual(str(error), 'The task type "invalid" is invalid!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_create_task_without_dates(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': None,
            'finish': None,
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        task_id = strategist.create_task(task_data)
        self.assertEqual(task_id, 1)
        retrieved_data = strategist.get_task(task_id)
        task_data['id'] = task_id
        self.assertEqual(retrieved_data, task_data)

    def test_create_task_invalid_date(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        try:
            _ = strategist.create_task(task_data)
        except ValueError as error:
            self.assertEqual(str(error), 'The start date cannot be after the finish date!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_get_task_invalid_id(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        _ = strategist.create_task(task_data)
        try:
            _ = strategist.get_task(1234)
        except ValueError as error:
            self.assertEqual(str(error), 'There is no task with id 1234!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_update_task(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        task_id = strategist.create_task(task_data)
        modified_data = {
            'name': 'Modified sample',
            'description': 'An other task for testing purposes',
            'type': 'quest',
            'start': datetime.datetime(2020, 8, 22, 10, 1, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 2, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        strategist.update_task(task_id, modified_data)
        retrieved_data = strategist.get_task(task_id)
        modified_data['id'] = task_id
        self.assertEqual(retrieved_data, modified_data)

    def test_update_task_invalid_id(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        _ = strategist.create_task(task_data)
        modified_data = {
            'name': 'Modified sample',
            'description': 'An other task for testing purposes',
            'type': 'quest',
            'start': datetime.datetime(2020, 8, 22, 10, 1, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 2, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        try:
            _ = strategist.update_task(1234, modified_data)
        except ValueError as error:
            self.assertEqual(str(error), 'There is no task with id 1234!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_update_task_invalid_type(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        task_id = strategist.create_task(task_data)
        modified_data = {
            'name': 'Modified sample',
            'description': 'An other task for testing purposes',
            'type': 'something wrong',
            'start': datetime.datetime(2020, 8, 22, 10, 1, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 2, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        try:
            _ = strategist.update_task(task_id, modified_data)
        except ValueError as error:
            self.assertEqual(str(error), 'The task type "something wrong" is invalid!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_update_task_invalid_date(self):
        strategist = Strategist('sqlite:///:memory:')
        task_data = {
            'name': 'Sample',
            'description': 'A sample task for testing purposes',
            'type': 'development',
            'start': datetime.datetime(2020, 8, 22, 10, 0, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 0, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        task_id = strategist.create_task(task_data)
        modified_data = {
            'name': 'Modified sample',
            'description': 'An other task for testing purposes',
            'type': 'quest',
            'start': datetime.datetime(2020, 8, 22, 20, 1, 0),
            'finish': datetime.datetime(2020, 8, 22, 12, 2, 0),
            'keywords': '',
            'owner_id': 1,
            'client_id': 1
        }
        try:
            _ = strategist.update_task(task_id, modified_data)
        except ValueError as error:
            self.assertEqual(str(error), 'The start date cannot be after the finish date!')
        else:
            self.fail('The expected ValueError has not raised!')

    def test_collect_in_interval(self):
        strategist = Strategist('sqlite:///:memory:')
        for h in range(1, 21):
            task_data = {
                'name': f'Sample {h}',
                'description': 'A sample task for testing purposes',
                'type': 'development',
                'start': datetime.datetime(2020, 8, 22, h, 0, 0),
                'finish': datetime.datetime(2020, 8, 22, h + 1, 0, 0),
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            _ = strategist.create_task(task_data)
        start = datetime.datetime(2020, 8, 22, 0, 0, 0)
        finish = datetime.datetime(2020, 8, 22, 21, 0, 0)
        tasks = strategist.collect_tasks_in_interval(start, finish)
        self.assertEqual(len(tasks), 20)
        for i, task in enumerate(tasks):
            self.assertEqual(task['start'].hour, i + 1)

    def test_collect_in_interval_inner(self):
        strategist = Strategist('sqlite:///:memory:')
        for h in range(1, 21):
            task_data = {
                'name': f'Sample {h}',
                'description': 'A sample task for testing purposes',
                'type': 'development',
                'start': datetime.datetime(2020, 8, 22, h, 0, 0),
                'finish': datetime.datetime(2020, 8, 22, h + 1, 0, 0),
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            _ = strategist.create_task(task_data)
        start = datetime.datetime(2020, 8, 22, 10, 0, 0)
        finish = datetime.datetime(2020, 8, 22, 14, 0, 0)
        tasks = strategist.collect_tasks_in_interval(start, finish)
        self.assertEqual(len(tasks), 5)
        for i, task in enumerate(tasks):
            print(task)
            self.assertEqual(task['start'].hour, i + 9)

    def test_collect_in_interval_empty(self):
        strategist = Strategist('sqlite:///:memory:')
        for h in range(5, 10):
            task_data = {
                'name': f'Sample {h}',
                'description': 'A sample task for testing purposes',
                'type': 'development',
                'start': datetime.datetime(2020, 8, 22, h, 0, 0),
                'finish': datetime.datetime(2020, 8, 22, h + 1, 0, 0),
                'keywords': '',
                'owner_id': 1,
                'client_id': 1
            }
            _ = strategist.create_task(task_data)
        start = datetime.datetime(2020, 8, 22, 12, 0, 0)
        finish = datetime.datetime(2020, 8, 22, 16, 0, 0)
        tasks = strategist.collect_tasks_in_interval(start, finish)
        self.assertEqual(tasks, [])

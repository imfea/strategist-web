import datetime
import hashlib
import json
import sys

import falcon
import jwt
import waitress

from strategist.strategist import Strategist


SECRET_KEY = '??????'


def encode_token(token):
    """
    Encode the session token for authentication.
    :param token: session token as a dictionary
    """
    encoded_token = jwt.encode(token, SECRET_KEY, algorithm='HS256')
    return encoded_token


def is_valid_token(encoded_token):
    """
    Check that the encoded session token is valid.
    :param encoded_token: session token in encoded form
    :return: True on valid token, else False
    """
    try:
        decoded_token = jwt.decode(encoded_token, SECRET_KEY, algorithms='HS256')
        if 'user_id' in decoded_token:
            return True
        else:
            return False
    except:
        return False


class LoginResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_post(self, req, resp):
        login_data = json.loads(req.stream.read().decode('utf-8'))
        email = login_data['email']
        password = hashlib.sha512(login_data['password'].encode('utf-8')).hexdigest()
        try:
            user_id = self._strategist.check_password(email, password)
            token = {
                'user_id': user_id
            }
            encoded_token = encode_token(token)
            response = {
                'token': 'Bearer ' + str(encoded_token)[2:-1]
            }
            resp.body = json.dumps(response)
            resp.status = falcon.HTTP_200
        except ValueError as error:
            resp.body = str(error)
            resp.status = falcon.HTTP_401


class TaskResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_get(self, req, resp, task_id):
        if 'AUTHORIZATION' not in req.headers:
            resp.status = falcon.HTTP_401
            return
        encoded_token = req.headers['AUTHORIZATION'][7:]
        if is_valid_token(encoded_token) is False:
            resp.status = falcon.HTTP_401
            return
        task_data = self._strategist.get_task(task_id)
        if task_data['start'] is not None:
            task_data['start'] = task_data['start'].strftime('%Y.%m.%d %H:%M:%S')
        else:
            task_data['start'] = ''
        if task_data['finish'] is not None:
            task_data['finish'] = task_data['finish'].strftime('%Y.%m.%d %H:%M:%S')
        else:
            task_data['finish'] = ''
        resp.body = json.dumps(task_data)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        if 'AUTHORIZATION' not in req.headers:
            resp.status = falcon.HTTP_401
            return
        encoded_token = req.headers['AUTHORIZATION'][7:]
        if is_valid_token(encoded_token) is False:
            resp.status = falcon.HTTP_401
            return
        task_data = json.loads(req.stream.read().decode('utf-8'))
        try:
            task_data['start'] =\
                datetime.datetime.strptime(task_data['start'], '%Y.%m.%d %H:%M:%S')
        except:
            task_data['start'] = None
        try:
            task_data['finish'] =\
                datetime.datetime.strptime(task_data['finish'], '%Y.%m.%d %H:%M:%S')
        except:
            task_data['finish'] = None
        task_data['keywords'] = ''
        task_data['owner_id'] = 1
        task_data['client_id'] = 1
        self._strategist.create_task(task_data)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200

    def on_put(self, req, resp, task_id):
        if 'AUTHORIZATION' not in req.headers:
            resp.status = falcon.HTTP_401
            return
        encoded_token = req.headers['AUTHORIZATION'][7:]
        if is_valid_token(encoded_token) is False:
            resp.status = falcon.HTTP_401
            return
        task_data = json.loads(req.stream.read().decode('utf-8'))
        task_data['id'] = task_id
        try:
            task_data['start'] =\
                datetime.datetime.strptime(task_data['start'], '%Y.%m.%d %H:%M:%S')
        except:
            task_data['start'] = None
        try:
            task_data['finish'] =\
                datetime.datetime.strptime(task_data['finish'], '%Y.%m.%d %H:%M:%S')
        except:
            task_data['finish'] = None
        task_data['keywords'] = ''
        task_data['owner_id'] = 1
        task_data['client_id'] = 1
        self._strategist.update_task(task_id, task_data)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class DayResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_get(self, req, resp, dateString):
        if 'AUTHORIZATION' not in req.headers:
            resp.status = falcon.HTTP_401
            return
        encoded_token = req.headers['AUTHORIZATION'][7:]
        if is_valid_token(encoded_token) is False:
            resp.status = falcon.HTTP_401
            return
        year = 2000 + int(dateString[0:2])
        month = int(dateString[2:4])
        day = int(dateString[4:6])
        start_date = datetime.datetime(year, month, day)
        finish_date = datetime.datetime(year, month, day) + datetime.timedelta(days=1)
        collected_tasks = self._strategist.collect_tasks_in_interval(start_date, finish_date)
        for i, task_data in enumerate(collected_tasks):
            collected_tasks[i]['start'] = task_data['start'].strftime('%Y.%m.%d %H:%M:%S')
            collected_tasks[i]['finish'] = task_data['finish'].strftime('%Y.%m.%d %H:%M:%S')
        resp.body = json.dumps(collected_tasks)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class QuoteResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_post(self, req, resp):
        if 'AUTHORIZATION' not in req.headers:
            resp.status = falcon.HTTP_401
            return
        encoded_token = req.headers['AUTHORIZATION'][7:]
        if is_valid_token(encoded_token) is False:
            resp.status = falcon.HTTP_401
            return
        quote_data = json.loads(req.stream.read().decode('utf-8'))
        self._strategist.create_quote(quote_data)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class GraphResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_get(self, req, resp, task_id):
        dependency_graph = self._strategist.calc_extended_dependency_graph(task_id)
        resp.body = json.dumps(dependency_graph)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class ProjectResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_get(self, req, resp):
        active_projects = self._strategist.collect_active_projects()
        resp.body = json.dumps(active_projects)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class SelectionResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_get(self, req, resp):
        selected_tasks = self._strategist.collect_selected_tasks()
        resp.body = json.dumps(selected_tasks)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        selection_data = json.loads(req.stream.read().decode('utf-8'))
        task_id = int(selection_data['task_id'])
        self._strategist.select_task(task_id)
        resp.body = ''
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        self._strategist.clear_task_selection()
        resp.body = ''
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class LatestResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_get(self, req, resp):
        latest_tasks = self._strategist.collect_latest_tasks()
        resp.body = json.dumps(latest_tasks)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class EdgeResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_post(self, req, resp):
        request_data = json.loads(req.stream.read().decode('utf-8'))
        print(request_data)
        try:
            source_id = request_data['source_id']
            target_id = request_data['target_id']
            if request_data['operation'] == 'add':
                print(f'{source_id} ---> {target_id}')
                self._strategist.add_edge(source_id, target_id)
            elif request_data['operation'] == 'remove':
                self._strategist.remove_edge(request_data['source_id'], request_data['target_id'])
        except Exception as error:
            print(error)
        resp.body = ''
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


class ActualResource:

    def __init__(self, strategist):
        self._strategist = strategist

    def on_get(self, req, resp):
        actual_ids = self._strategist.get_actuals()
        actuals = []
        for actual_id in actual_ids:
            task_data = self._strategist.get_task(actual_id)
            task_data['start'] = str(task_data['start'])
            task_data['finish'] = str(task_data['finish'])
            actuals.append(task_data)
        resp.body = json.dumps(actuals)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        actuals = json.loads(req.stream.read().decode('utf-8'))
        actuals = self._strategist.set_actuals(actuals)
        resp.body = ''
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


if __name__ == '__main__':
    strategist = Strategist('sqlite:///strategist.db3')

    login_resource = LoginResource(strategist)
    task_resource = TaskResource(strategist)
    day_resource = DayResource(strategist)
    quote_resource = QuoteResource(strategist)
    graph_resource = GraphResource(strategist)
    project_resource = ProjectResource(strategist)
    selection_resource = SelectionResource(strategist)
    latest_resource = LatestResource(strategist)
    edge_resource = EdgeResource(strategist)
    actual_resource = ActualResource(strategist)

    app = falcon.API()

    app.add_route('/api/login', login_resource)
    app.add_route('/api/tasks', task_resource)
    app.add_route('/api/tasks/{task_id:int}', task_resource)
    app.add_route('/api/day/{dateString}', day_resource)
    app.add_route('/api/quotes', quote_resource)
    app.add_route('/api/graph/{task_id:int}', graph_resource)
    app.add_route('/api/projects', project_resource)
    app.add_route('/api/selections', selection_resource)
    app.add_route('/api/latests', latest_resource)
    app.add_route('/api/edges', edge_resource)
    app.add_route('/api/actuals', actual_resource)

    if len(sys.argv) == 2:
        port = int(sys.argv[1])
    else:
        port = 5000
    waitress.serve(app, host='0.0.0.0', port=port, threads=1)


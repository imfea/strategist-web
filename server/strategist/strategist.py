import json

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from strategist.database import create_session
from strategist.model import Task
from strategist.model import Edge
from strategist.model import Register
from strategist.model import Selection
from strategist.model import Quote
from strategist.model import User


task_types = [
    "administration",
    "artistic",
    "configuration",
    "consultation",
    "debit",
    "design",
    "development",
    "documentation",
    "freetime",
    "lecture",
    "mail",
    "plan",
    "private",
    "project",
    "quest",
    "requirement",
    "study",
    "teach",
    "travel"
]


class Strategist:
    """
    Strategist application primary interface
    """
    
    def __init__(self, connection_string):
        """
        Initialize a new strategist instance.
        :path connection_string: database dialect and connection descriptor
        :return: None
        """
        self._session = create_session(connection_string)

    def check_password(self, email, password):
        """
        Check the hashed password.
        :param email: unique email address of the user
        :param password: password in hashed form
        :return: user identifier on valid email address and password pair
        :raise ValueError: on invalid email address or password
        """
        try:
            user = self._session.query(User).filter_by(email=email).one()
            if user.password == password:
                return user.id
            else:
                raise ValueError(f'Invalid email address or password!')
        except NoResultFound:
            raise ValueError(f'Invalid email address or password!')

    @staticmethod
    def check_task_data(task_data):
        """
        Check that the task data are valid.
        :param task_data: task data as a dictionary
        :return: None
        :raise ValueError: on invalid task data
        """
        if task_data['type'] not in task_types:
            invalid_type = task_data['type']
            raise ValueError(f'The task type "{invalid_type}" is invalid!')
        if task_data['start'] and task_data['finish']:
            if task_data['start'] > task_data['finish']:
                raise ValueError('The start date cannot be after the finish date!')

    def create_task(self, task_data):
        """
        Create a new task.
        :param task_data: task data as a dictionary
        :return: unique identifier of the new task
        :raise ValueError: on invalid task data
        """
        self.check_task_data(task_data)
        task = Task(**task_data)
        self._session.add(task)
        self._session.commit()
        return task.id

    def is_valid_task_id(self, task_id):
        """
        Check that the task id is valid.
        :param task_id: unique identifier of the task
        :return: True on valid task, else False
        """
        task = self._session.query(Task).filter_by(id=task_id).first()
        return task is not None

    def get_task(self, task_id):
        """
        Get the data of the task from the database.
        :param task_id: unique identifier of the task
        :return: task data in a dictionary
        :raise ValueError: on invalid task identifier
        """
        try:
            task = self._session.query(Task).filter_by(id=task_id).one()
            return task.to_dictionary()
        except NoResultFound:
            raise ValueError(f'There is no task with id {task_id}!')

    def update_task(self, task_id, task_data):
        """
        Update the data of the given task.
        :param task_id: unique identifier of the task
        :param task_data: task data as a dictionary
        :return: None
        :raise ValueError: on invalid task identifier or task data
        """
        self.check_task_data(task_data)
        try:
            task = self._session.query(Task).filter_by(id=task_id).one()
            task.name = task_data['name']
            task.description = task_data['description']
            task.type = task_data['type']
            task.start = task_data['start']
            task.finish = task_data['finish']
            task.keywords = task_data['keywords']
            task.owner = task_data['owner_id']
            task.client = task_data['client_id']
        except NoResultFound:
            raise ValueError(f'There is no task with id {task_id}!')
    
    def destroy_task(self, task_id):
        """
        Remove the given task from the database.
        :param task_id: unique identifier of the task
        :return: None
        :raise ValueError: on invalid task identifier
        """
        pass

    def collect_tasks_in_interval(self, start_date, finish_date):
        """
        Collect tasks in the given time interval.
        :param start_date: start of the interval (inclusive)
        :param finish_date: end of the interval (exclusive)
        :return: list of dictionaries with task data
        :raise ValueError: on invalid interval
        """
        result_tasks = []
        tasks = self._session.query(Task).\
            filter(Task.start < finish_date, Task.finish >= start_date).\
            order_by(Task.start).\
            all()
        for task in tasks:
            task_data = task.to_dictionary()
            result_tasks.append(task_data)
        return result_tasks

    def create_quote(self, quote_data):
        """
        Create a new quote.
        :param quote_data: quote data as a dictionary
        :return: unique identifier of the new quote
        :raise ValueError: on invalid quote data
        """
        quote = Quote(**quote_data)
        self._session.add(quote)
        self._session.commit()
        return quote.id

    def add_edge(self, source_id, target_id):
        """
        Add a new edge to the dependency graph.
        :param source_id: task identifier of the source node
        :param target_id: task identifier of the target node
        :return: None
        :raise ValueError: on invalid task identifier
        """
        if self.is_valid_task_id(source_id) is False:
            raise ValueError(f'The task id {source_id} is invalid for adding edge!')
        if self.is_valid_task_id(target_id) is False:
            raise ValueError(f'The task id {target_id} is invalid for adding edge!')
        if source_id == target_id:
            raise ValueError(f'Unable to add edge {source_id} -> {target_id}!')
        graph = self.calc_dependency_graph(source_id)
        if target_id in graph:
            raise ValueError(f'Unable to add edge {source_id} -> {target_id}!')
        edge = Edge(source_id=source_id, target_id=target_id)
        try:
            self._session.add(edge)
            self._session.commit()
            print('OK')
        except IntegrityError as integrity_error:
            self._session.rollback()
            print(integrity_error)

    def remove_edge(self, source_id, target_id):
        """
        Remove an edge from the dependency graph.
        :param source_id: task identifier of the source node
        :param target_id: task identifier of the target node
        :return: None
        :raise ValueError: on invalid task identifier
        """
        edge = self._session.query(Edge).\
            filter(Edge.source_id == source_id, Edge.target_id == target_id).\
            first()
        if edge is not None:
            self._session.delete(edge)
            self._session.commit()

    def collect_predecessors(self, task_id):
        """
        Collect the predecessors of the given task.
        :param task_id: task identifier
        :return: list of predecessor task identifiers
        :raise ValueError: on invalid task identifier
        """
        if self.is_valid_task_id(task_id) is False:
            raise ValueError(f'The task id {task_id} is invalid for collecting predecessors!')
        edges = self._session.query(Edge).\
            filter(Edge.target_id == task_id).\
            all()
        predecessors = [edge.source_id for edge in edges]
        return predecessors

    def collect_successors(self, task_id):
        """
        Collect the successors of the given task.
        :param task_id: task identifier
        :return: list of successor task identifiers
        :raise ValueError: on invalid task identifier
        """
        if self.is_valid_task_id(task_id) is False:
            raise ValueError(f'The task id {task_id} is invalid for collecting successors!')
        edges = self._session.query(Edge).\
            filter(Edge.source_id == task_id).\
            all()
        successors = [edge.target_id for edge in edges]
        return successors

    def calc_extended_dependency_graph(self, root_task_id):
        """
        Calculate the extended dependency graph for the given task node.
        :param root_task_id: task identifier
        :return: dictionary of task items where the key is the task_id
        :raise ValueError: on invalid task identifier
        """
        dependency_graph = self.calc_dependency_graph(root_task_id)
        extended_dependency_graph = self.extend_dependency_graph(dependency_graph)
        return extended_dependency_graph

    def calc_dependency_graph(self, root_task_id):
        """
        Calculate the dependency graph for the given task node.
        :param root_task_id: task identifier
        :return: dictionary of task items where the key is the task_id
        :raise ValueError: on invalid task identifier
        """
        if self.is_valid_task_id(root_task_id) is False:
            raise ValueError(f'The task id {root_task_id} is invalid for calculating dependency graph!')
        graph = {}
        unchecked_task_ids = [root_task_id]
        while unchecked_task_ids:
            task_id = unchecked_task_ids.pop()
            if task_id not in graph:
                predecessors = self.collect_predecessors(task_id)
                successors = self.collect_successors(task_id)
                graph[task_id] = {
                    'predecessors': predecessors,
                    'successors': successors
                }
                unchecked_task_ids.extend(predecessors)
        root_successors = self.collect_successors(root_task_id)
        for task_id in root_successors:
            graph[task_id] = {
                'predecessors': [],
                'successors': []
            }
        return graph

    def extend_dependency_graph(self, dependency_graph):
        """
        Extend the dependency graph with task data.
        :param dependency_graph: dictionary of task items where the key is the task_id
        :return: dictionary of task items where the key is the task_id
        """
        for task_id in dependency_graph.keys():
            task_data = self.get_task(task_id)
            task_data['start'] = str(task_data['start'])
            task_data['finish'] = str(task_data['finish'])
            dependency_graph[task_id].update(task_data)
        return dependency_graph

    def collect_latest_tasks(self):
        """
        Collect the recently created tasks
        :return: list of dictionaries with task data
        """
        # TODO: Use limit from configuration!
        tasks = self._session.query(Task).order_by(Task.creation.desc()).limit(32).all()
        latest_tasks = []
        for task in tasks:
            task_data = task.to_dictionary()
            task_data['start'] = str(task_data['start'])
            task_data['finish'] = str(task_data['finish'])
            latest_tasks.append(task_data)
        return latest_tasks

    def create_register(self, user_id):
        """
        Create a new register for the given user.
        :param user_id: user identifier
        :return: None
        """
        register = Register(user_id=user_id)
        self._session.add(register)
        self._session.commit()

    def get_actuals(self):
        """
        Get the list of actual task identifiers.
        :return: list of actual task identifiers
        """
        register = self._session.query(Register).filter(Register.user_id == 1).one()
        actuals = json.loads(register.actuals)
        return actuals

    def set_actuals(self, actuals):
        """
        Set the list of actual task identifiers.
        :param actuals: list of actual task identifiers
        :return: None
        """
        register = self._session.query(Register).filter(Register.user_id == 1).one()
        register.actuals = json.dumps(actuals)
        self._session.commit()

    def collect_active_projects(self):
        """
        Collect tasks with type project which have not finished yet.
        :return: list of active projects
        """
        tasks = self._session.query(Task).\
            filter(Task.type == 'project', Task.finish == None).\
            order_by(Task.id).\
            all()
        return [task.to_dictionary() for task in tasks]

    def select_task(self, task_id):
        """
        Add the given task to the selection.
        :param task_id: task identifier
        :return: None
        """
        if self.is_valid_task_id(task_id) is False:
            raise ValueError(f'The task id {task_id} is invalid!')
        try:
            # TODO: Temporary fix! Remove here!
            self.deselect_task(task_id)
        except:
            pass
        selection = Selection(task_id=task_id)
        self._session.add(selection)
        self._session.commit()

    def deselect_task(self, task_id):
        """
        Remove the given task from the selection.
        :param task_id: task identifier
        :return: None
        """
        if self.is_valid_task_id(task_id) is False:
            raise ValueError(f'The task id {task_id} is invalid!')
        try:
            selection = self._session.query(Selection).\
                filter(Selection.task_id == task_id).\
                one()
            self._session.delete(selection)
            self._session.commit()
        except NoResultFound:
            raise ValueError(f'The task with id {task_id} has not selected!')

    def collect_selected_tasks(self):
        """
        Collect the selected tasks.
        :return: list of selected task data in dictionaries
        """
        selected_tasks = []
        selections = self._session.query(Selection).\
            order_by(Selection.creation.desc()).\
            all()
        for selection in selections:
            task = self.get_task(selection.task_id)
            task['start'] = str(task['start'])
            task['finish'] = str(task['finish'])
            selected_tasks.append(task)
        return selected_tasks

    def clear_task_selection(self):
        """
        Remove all tasks from the selection.
        :return: None
        """
        self._session.query(Selection).delete()

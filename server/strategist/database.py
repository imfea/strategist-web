from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from strategist.model import Base


def create_session(connection_string):
    """
    Create a new database session.
    :path connection_string: database dialect and connection descriptor
    :return: database session object
    """
    engine = create_engine(connection_string, echo=False)
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


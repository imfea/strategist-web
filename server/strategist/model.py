import datetime

from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.sqlite import DATETIME

dt = DATETIME(
    storage_format='%(year)04d-%(month)02d-%(day)02dT'
                   '%(hour)02d:%(minute)02d:%(second)02d.%(microsecond)06d',
    regexp=r'(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+).(\d+)'
)

Base = declarative_base()

DEPENDENCY = 0
PRIORITY = 1


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    email = Column(String)
    password = Column(String)


class Task(Base):
    __tablename__ = 'tasks'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)
    type = Column(String)
    start = Column(dt)
    finish = Column(dt)
    keywords = Column(String)
    owner_id = Column(Integer)
    client_id = Column(Integer)
    creation = Column(dt, default=datetime.datetime.utcnow)

    def to_dictionary(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'type': self.type,
            'start': self.start,
            'finish': self.finish,
            'keywords': self.keywords,
            'owner_id': self.owner_id,
            'client_id': self.client_id
        }


class Edge(Base):
    __tablename__ = 'edges'

    source_id = Column(Integer, primary_key=True)
    target_id = Column(Integer, primary_key=True)
    type = Column(Integer, nullable=False, default=0)


class Selection(Base):
    __tablename__ = 'selections'

    task_id = Column(Integer, primary_key=True)
    creation = Column(dt, default=datetime.datetime.utcnow)


class Register(Base):
    __tablename__ = 'registers'

    user_id = Column(Integer, primary_key=True)
    actuals = Column(String, nullable=False, default='[]')


class Quote(Base):
    __tablename__ = 'quotes'

    id = Column(Integer, primary_key=True)
    content = Column(String)
    creation = Column(dt, default=datetime.datetime.utcnow)
